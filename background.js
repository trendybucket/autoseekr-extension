var SCRIPTID = "1OyjlytEo2uEorwa12PyFanPBm549L9Koe2Fl5Mwkg-p-5V4k7k-KgDsc";
/// https://script.google.com/macros/s/AKfycbxB5hPATZclDz6hwe-HKi5o-g9bXtnug6x9nNT5zyBlyeY0pB0/exec
//// !!! ONLY FOR TESTING....!
var o1 = "1TBqN2KpOMse7_pyKDSZW1QIFii1-5VAL9Zbz2np_TUM";
var o2 = "14850XpEs5bNWo8RttAMgUdvA4LE1UK2mFEhPpI9wk18";
var o3 = "1yOQqQ2Nwae5xvS2tuGbUNGUIRqGwqhWw";

var STATE_START = 1;
var STATE_ACQUIRING_AUTHTOKEN = 2;
var STATE_AUTHTOKEN_ACQUIRED = 3;
var state = STATE_START;

browser.runtime.onMessage.addListener(function (request, sender, sendResponse)
{
	console.log(sender);
	console.log(request);

	var msg = message.msg;
	if (msg == "getSettings") {
		sendResponse({ "msg": "settings", "sht": "ss", "tpl": "tt", "fld": "ff" });
	}
	if (msg == "newUrl") {
		var aurl = message.url;
		////
	}
	if (msg == "getFields") {
		var aurl = message.url;
		sendResponse({ "msg": "fields", "url": aurl });
	}
});

function sendLoad(msg, which)
{
	if (msg == 'on') {
		browser.runtime.sendMessage({
			msg: 'load',
			load: 'on',
			which: which
		});
	}
	else if (msg == 'off') {
		browser.runtime.sendMessage({
			msg: 'load',
			load: 'off'
		});
	}
}
function changeState(newState)
{
	state = newState;
	sendStateChg(state);
	switch (state) {
		case STATE_START:
			authd = false;
			browser.contextMenus.update('SignIn', {
				visible: true
			});
			browser.contextMenus.update('Revoke', {
				visible: false
			});
			browser.contextMenus.update('SendToSheet', {
				visible: false
			});
			browser.contextMenus.update('GoToSheet', {
				visible: false
			});
			break;
		case STATE_ACQUIRING_AUTHTOKEN:
			sendLog('Acquiring token...');
			sendLoad('on', '#spin1');
			sendLoad('on', '#spin3');
			browser.contextMenus.update('SignIn', {
				title: "SIGNING IN..."
			});
			sendLoad('on', 'auth');
			break;
		case STATE_AUTHTOKEN_ACQUIRED:
			sendLoad('off', '');
			authd = true;
			browser.contextMenus.update('SignIn', {
				visible: false
			});
			browser.contextMenus.update('Revoke', {
				visible: true
			});
			browser.contextMenus.update('SendToSheet', {
				visible: true
			});
			browser.contextMenus.update('GoToSheet', {
				visible: true
			});
			break;
	}
}
function sendOptsToSheet(token)
{
	alert('sending ids to Sheet');
	post({
		'url': 'https://script.googleapis.com/v1/scripts/' + SCRIPT_ID +
			':run',
		'callback': executionAPIResponse,
		'token': token,
		'request': {
			'function': 'setIds',
			'parameters': {
				'data': JSON.parse(newIds)
			}
		}
	});
}
function sendVals()
{
	var dat = getSet();
	var thedat = object.jobAppFields;
	jobAppFields = thedat;
	//"[" + viObj[0] + "\,\"" + viObj[1] + "\",\"" + viObj[2] + "\",\"" + viObj[3] + "\",\"" + viObj[4] + "\",\"" + viObj[5] + "\",\"" + viObj[6] + "\"]]";
	getAuthToken({
		'interactive': false,
		'callback': sendValsToSheet
	});
}
//// AUTH FUNCTIONS
function getAuthToken(options)
{
	browser.identity.getAuthToken({
		'interactive': options.interactive
	}, options.callback);
}
function getAuthTokenSilent()
{
	getAuthToken({
		'interactive': false,
		'callback': getAuthTokenCallback
	});
}
function getAuthTokenInteractive()
{
	alert('signing in...');
	getAuthToken({
		'interactive': true,
		'callback': getAuthTokenCallback
	});
}
function getAuthTokenCallback(token)
{
	if (browser.runtime.lastError) {
		alert('No token aquired');
		changeState(STATE_START);
	}
	else {
		alert('Logged In');
		browser.contextMenus.update('SignIn', {
			visible: false
		});
		browser.contextMenus.update('RevokeToken', {
			visible: true
		});
		browser.contextMenus.update('Send2Sheet', {
			visible: true
		});
		browser.contextMenus.update('Go2Sheet', {
			visible: true
		});
		changeState(STATE_AUTHTOKEN_ACQUIRED);
	}
}
function executionAPIResponse(response)
{
	var resp = JSON.stringify(response);
	alert(resp);
	var info;
	if (response.response.result.status == 'ok') {
		sendLog('Data has been entered into <a href="' + response.response
			.result
			.doc + '" target="_blank"><strong>this sheet</strong></a>');
	}
	else {
		sendLog('Error...');
	}
	sendLoad('off', '');
}
function revokeToken()
{
	getAuthToken({
		'interactive': false,
		'callback': revokeAuthTokenCallback,
	});
}
function revokeAuthTokenCallback(current_token)
{
	if (!browser.runtime.lastError) {
		browser.identity.removeCachedAuthToken({
			token: current_token
		}, function () { });
		var xhr = new XMLHttpRequest();
		xhr.open('GET', 'https://accounts.google.com/o/oauth2/revoke?token=' +
			current_token);
		xhr.send();
		changeState(STATE_START);
		sendLog(
			'Token revoked and removed from cache. browser://identity-internals to confirm.'
		);
	}
	sendLoad('off', '');
}
function post(options)
{
	sendLog('posting');
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function ()
	{
		if (xhr.readyState === 4 && xhr.status === 200) {
			// JSON response assumed. Other APIs may have different responses.
			options.callback(JSON.parse(xhr.responseText));
		}
		else if (xhr.readyState === 4 && xhr.status !== 200) {
			sendLog('post', xhr.readyState, xhr.status, xhr.responseText);
		}
	};
	xhr.open('POST', options.url, true);
	xhr.setRequestHeader('Authorization', 'Bearer ' + options.token);
	xhr.send(JSON.stringify(options.request));
}


// CONTEXT MENUS
browser.runtime.onInstalled.addListener(function ()
{
	var parent = browser.contextMenus.create({
		title: "autoSEEKr",
		id: "parent",
		contexts: ['all']
	});
	browser.contextMenus.create({
		id: "SignIn",
		parentId: parent,
		title: "SignIn",
		contexts: ["all"]
	});
	browser.contextMenus.create({
		id: "RevokeToken",
		parentId: parent,
		title: "RevokeToken",
		contexts: ["all"],
		visible: false
	});
	browser.contextMenus.create({
		id: 's1',
		parentId: parent,
		type: 'separator',
		contexts: ['all']
	});
	browser.contextMenus.create({
		id: "Send2Sheet",
		parentId: parent,
		title: "Send2Sheet",
		contexts: ["all"],
		visible: false
	});
	browser.contextMenus.create({
		id: "GoToSheet",
		parentId: parent,
		title: "GoToSheet",
		contexts: ["all"],
		visible: false
	});
	browser.contextMenus.create({
		id: 's2',
		parentId: parent,
		type: 'separator',
		contexts: ['all']
	});
	for (var k = 0; k < theTitles.length; k++) {
		var key = theTitles[k];
		browser.contextMenus.create({
			id: key,
			parentId: parent,
			title: key,
			contexts: ['selection']
		});
	}
	browser.contextMenus.create({
		id: "ResetFields",
		parentId: parent,
		title: "ResetFields",
		contexts: ['all'],
		visible: false
	});
	//	browser.contextMenus.create({id: "Agency", parentId: parent,title: "Agency","contexts": ["all"],"type": "checkbox"});
});
browser.contextMenus.onClicked.addListener(function (item, tab)
{
	var url = tab.url;
	alert(url);
	var theField = item.menuItemId;
	var theValue = item.selectionText;
	jobAppFields.Url = url;
	for (var c = 0; c < theTitles.length; c++) {
		var dis = theTitles[c];
		if (theField == dis) {
			jobAppFields["dis"] = theValue;
			browser.contextMenus.update(theField, {
				title: theField + ": *" + theValue + "*"
			});
			browser.storage.local.set({
				jobAppFields
			});
		}
	}
	if (theField == 'Send2Sheet') {
		sendVals();
	}
	else if (theField == 'SignIn') {

		getAuthTokenInteractive();
	}
	else if (theField == 'GoToSheet') {
		var url = "https://docs.google.com/spreadsheets/d/" + o1 + "/edit";
		browser.tabs.create({
			url: url,
			index: tab.index + 1
		});
	}
	else if (theField == 'ResetFields') {
		resetIt();
	}
	else if (theField == 'RevokeToken') {
		revokeToken();
	}
	else {
		var n = url.search("seek.com.au");
		if (n !== "-1") {



		}
	}
});
