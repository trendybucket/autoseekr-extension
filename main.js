const electron = require('electron')
const path = require('path')
const BrowserWindow = electron.remote.BrowserWindow
const axios = require('axios')
const ipc = electron.ipcRenderer

const notifyBtn = document.querySelector('#notifyBtn')
const goButton = document.querySelector('#go')
const reset = document.querySelector('#reset')
const close = document.querySelector('#close')
var newJobBtn = document.querySelector('#newJob')
var sht = document.querySelector('#targetPrice')
var targetPriceVal

const notification = {
	title: 'New Job Completed',
	body: 'BTC just beat your target price!',
	icon: path.join(__dirname, '../assets/images/gear.png')
}

setInterval(getJobSheet, 10000);

newJob.addEventListener('click', function (event)
{
	const modalPath = path.join('file://', __dirname, 'fields.html')
	let win = new BrowserWindow({
		frame: false,
		transparent: true,
		alwaysOnTop: true,
		width: 400,
		height: 200,
		x: 200,
		y: 200
	})
	win.on('close', function ()
	{
		win = null
	})
	win.loadURL(modalPath)
	win.show()
})

ipc.on('fillField', function (event, arg)
{
	field = arg.field
	valu = arg.valu
	let thefield = '#' + field
	document.querySelector(thefield).innerText = valu
	/// STORE VALUE !!!
	////
})
