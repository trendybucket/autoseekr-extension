// https://script.google.com/macros/s/AKfycbxB5hPATZclDz6hwe-HKi5o-g9bXtnug6x9nNT5zyBlyeY0pB0/exec
//API Key		MXmsJN0_nH3qsA5u644e9P8lvjbIvZS7r

browser.runtime.onMessage.addListener(notify);
function notify(message)
{
	var msg = message.msg;
	if (msg === "settings") {
		var sheet = message.sht;
		var folder = message.fld;
		var template = message.tpl;
		document.querySelector('#shtin').innerText = sheet;
		document.querySelector('#tplin').innerText = template;
		document.querySelector('#fldin').innerText = folder;
	}
	if (msg === "fields") {
		var aurl = message.url;
		var jt = message.jt;
		var emp = message.emp;
		var u1 = message.usp1;
		var u2 = message.usp2;
		var u3 = message.usp3;
	}
}
function getSettings()
{
	browser.runtime.sendMessage({ "msg": "getSettings" });
}
function getFields()
{
	browser.runtime.sendMessage({ "msg": "getFields" });
}
function disableButton(button)
{
	button.setAttribute('disabled', 'disabled');
}
function enableButton(button)
{
	button.removeAttribute('disabled');
}
function closeWindow()
{
	window.close();
}
function notifyBkpage(e)
{
	// ONLY NOTIFY FOR <a> TAGS
	if (e.target.tagName != "A") {
		return;
	}
	browser.runtime.sendMessage({ "msg": "newUrl", "url": e.target.href });
}
window.addEventListener("click", notifyBkpage);
function paste(theFieldwHashInFront)
{
	var pasteText = document.querySelector(theFieldwHashInFront);
	navigator.clipboard.readText().then(clipText =>
		pasteText.innerText += clipText);
}

document.addEventListener('DOMContentLoaded', function ()
{
	var sidenavs = document.querySelectorAll('.sidenav')
	for (var i = 0; i < sidenavs.length; i++) {
		M.Sidenav.init(sidenavs[i]);
	}
	var dropdowns = document.querySelectorAll('.dropdown-trigger')
	for (var i = 0; i < dropdowns.length; i++) {
		M.Dropdown.init(dropdowns[i]);
	}
	var collapsibles = document.querySelectorAll('.collapsible')
	for (var i = 0; i < collapsibles.length; i++) {
		M.Collapsible.init(collapsibles[i]);
	}
	var featureDiscoveries = document.querySelectorAll('.tap-target')
	for (var i = 0; i < featureDiscoveries.length; i++) {
		M.FeatureDiscovery.init(featureDiscoveries[i]);
	}
	var materialboxes = document.querySelectorAll('.materialboxed')
	for (var i = 0; i < materialboxes.length; i++) {
		M.Materialbox.init(materialboxes[i]);
	}
	var modals = document.querySelectorAll('.modal')
	for (var i = 0; i < modals.length; i++) {
		M.Modal.init(modals[i]);
	}
	var parallax = document.querySelectorAll('.parallax')
	for (var i = 0; i < parallax.length; i++) {
		M.Parallax.init(parallax[i]);
	}
	var scrollspies = document.querySelectorAll('.scrollspy')
	for (var i = 0; i < scrollspies.length; i++) {
		M.ScrollSpy.init(scrollspies[i]);
	}
	var tabs = document.querySelectorAll('.tabs')
	for (var i = 0; i < tabs.length; i++) {
		M.Tabs.init(tabs[i]);
	}
	var tooltips = document.querySelectorAll('.tooltipped')
	for (var i = 0; i < tooltips.length; i++) {
		M.Tooltip.init(tooltips[i]);
	}
})

document.querySelector("#setFromClip").addEventListener("click", paste);
document.querySelector("#but").addEventListener("click", hit);
document.querySelector("#but2").addEventListener("click", paste);
document.querySelector("#setFromClip").addEventListener("click", paste);



